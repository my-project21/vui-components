// Libraries
import React from 'react';
import styled from 'styled-components';

// Util
// import {responsive} from '../_util/utils.js';

type propsRow = {
    children?: React.ReactNode;
    justify?: 'center' | 'start' | 'flex-end' | 'space-between' | 'space-around';
    align?: 'top' | 'center' | 'bottom';
    space?: [number, number] | number;
    responsive?: {};
}

const RowComponent = styled.div`
    display: flex;
    position: relative;
    align-items: ${(props: propsRow) => {
        switch (props.align) {
            case 'top':
                return 'flex-start';
            case 'center':
                return 'center';
            case 'bottom':
                return 'flex-end';
            default:
                return 'center';
        }
    }};
    gap: ${(props: propsRow) => formatGap(props.space)};
    flex-flow: row wrap;
    justify-content: center;
`;

const formatGap = (space: [number, number] | number) => {
    if (Array.isArray(space)) {
        return `${space[0]}px ${space[1]}px`;
    } else if (typeof space === 'number') {
        return space + 'px';
    } else {
        return '0px';
    }
};

export function Row(props: propsRow) {
    // Props
    const {children, justify = 'center', align = 'center', space, responsive} = props;

    return (
        <RowComponent style={{justifyContent: justify}} align={align} space={space} responsive={responsive}>
            {children && children}
        </RowComponent>
    );
}
