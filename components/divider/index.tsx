// Libraries
import React from 'react';
import styled from 'styled-components';

// Utils
import {getObjectPropSafely} from '../_util/utils.js';

const cn = require('classnames');

// Styles
import styles from './styles.module.scss';

const DividerTitle = styled.div`
    width: 100%;
    align-items: center;
    display: flex;
    margin: 10px 0px;
    white-space: nowrap;

    &.center {
        &::before {
            content: '';
            width: 50%;
            border-top: 1px solid ${(props) => getObjectPropSafely(() => props.color, '#bfbfbf')};
            top: 50%;
            margin: auto;
        }
        &::after {
            content: '';
            width: 50%;
            top: 50%;
            border-top: 1px solid ${(props) => getObjectPropSafely(() => props.color, '#bfbfbf')};
            margin: auto;
        }
    }

    &.left {
        &::before {
            content: '';
            width: 5%;
            top: 50%;
            border-top: 1px solid${(props) => getObjectPropSafely(() => props.color, '#bfbfbf')};
            margin: auto;
        }
        &::after {
            content: '';
            width: 95%;
            top: 50%;
            border-top: 1px solid ${(props) => getObjectPropSafely(() => props.color, '#bfbfbf')};
            margin: auto;
        }
    }

    &.right {
        &::before {
            content: '';
            width: 95%;
            top: 50%;
            border-top: 1px solid ${(props) => getObjectPropSafely(() => props.color, '#bfbfbf')};
            margin: auto;
        }
        &::after {
            content: '';
            width: 5%;
            top: 50%;
            border-top: 1px solid ${(props) => getObjectPropSafely(() => props.color, '#bfbfbf')};
            margin: auto;
        }
    }

    .title {
        font-weight: bold;
        padding: 0 1em;
    }
`;

// Types
type propsDivider = {
    style?: React.CSSProperties,
    placement?: 'left' | 'right' | 'center',
    children?: React.ReactNode | string | number;
    normal?: boolean;
    className?: string;
    styleDivider?: 'solid' | 'dashed' | 'dotted';
    color?: string;
    dividerColor?: string;
    type?: 'vertical' | 'horizontal';
}

function Divider(props: propsDivider) {
    const {style = {}, placement = 'center', children, dividerColor = '#bfbfbf', normal = false, className = '', type = 'horizontal', styleDivider = 'solid', color = '#bfbfbf'} = props;
    
    return (
        <>
            {!children ? (
                <div
                    color={'red'}
                    style={{...style, borderStyle: styleDivider, borderColor: color}} 
                    className={
                        cn(styles['divider'], className, styles[type])
                    } 
                />
            ) :
                <>
                    <DividerTitle className={placement} color={dividerColor} >
                        <div className='title' style={normal ? {...style, fontWeight: 'normal', fontSize: 14} : {...style}}>{children}</div>
                    </DividerTitle>
                </>
            }
        </>
    );
}

Divider.defaultProps = {
    style: {}
};

export default Divider;

