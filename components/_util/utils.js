export const getObjectPropSafely = (fn, defaultValue = '') => {
    try {
        return fn();
    } catch (e) {
        return defaultValue;
    }
};

export const responsive = [
    {name: 'xs', value: '0px'},
    {name: 'sm', value: '576px'},
    {name: 'md', value: '768px'},
    {name: 'lg', value: '992px'},
    {name: 'xl', value: '1200px'},
    {name: 'xxl', value: '1600px'}
];