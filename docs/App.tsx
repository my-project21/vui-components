import React from 'react';
import './styles.less';
import {Button, Divider, Icon, notification, Row} from '../components';

const App = () => {

    const onClick = () => {
        notification.warning({
            title: 'Hello'
        });
    };

    return (
        <div style={{height: 10000}}>
            <Button onClick={onClick} type='dashed' >Button</Button>
            <div style={{display: 'flex', alignItems: 'center'}}>
                <Divider placement='center'>dqw</Divider>
            </div>
            <Icon />
            <Row justify='center' align='center' space={[20, 20]}>
                <div className='box box-1'>2</div>
                <div className='box box-2'>3</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
                <div className='box box-3'>4</div>
            </Row>
        </div>
    );
};

export default App;
